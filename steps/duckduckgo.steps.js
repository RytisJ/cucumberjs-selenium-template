const {Given, When, Then} = require('@cucumber/cucumber');

const DuckDuckGoPage = require('../pages/duckduckgo.page');
let duckDuckGoPage;

Given('I go to duckduckgo', async function() {
  duckDuckGoPage = new DuckDuckGoPage(this.driver);
});
When('I search fo {string}', async (query) =>
  await duckDuckGoPage.enterQuery(query));
Then('the page title is {string}', async function(expected) {
  this.expect(await duckDuckGoPage.pageTitle()).to.equal(expected);
});
