const BasePage = require('./base.page');
const {By} = require('selenium-webdriver');
const config = require('config');

class DuckduckgoPage extends BasePage {
  constructor(driver) {
    super(driver);
    this.driver.get(config.get('duckduckgo'));
  }

  async enterQuery(query) {
    await this.sendKeys(By.id('search_form_input_homepage'), query);
    await this.click(By.id('search_button_homepage'));
  }

  async pageTitle() {
    await this.pageIsLoaded();
    return this.driver.getTitle();
  }
}

module.exports = DuckduckgoPage;
