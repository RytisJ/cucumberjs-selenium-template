const {until} = require('selenium-webdriver');

class BasePage {
  constructor(driver) {
    this.driver = driver;
  }

  waitForElement(selector) {
    this.driver.wait(until.elementLocated(selector));
  }

  async sendKeys(selector, value) {
    this.waitForElement(selector);
    await this.driver.findElement(selector).sendKeys(value);
  }

  async click(selector) {
    this.waitForElement(selector);
    await this.driver.findElement(selector).click();
  }

  async sleepUntil(f, timeoutMs) {
    return new Promise((resolve, reject) => {
      const timeWas = new Date();
      const wait = setInterval(function() {
        if (f()) {
          clearInterval(wait);
          resolve();
        } else if (new Date() - timeWas > timeoutMs) { // Timeout
          clearInterval(wait);
          reject(new Error('Page did not load in time'));
        }
      }, 20);
    });
  }

  async pageIsLoaded() {
    await this.sleepUntil(
        () => this.driver.executeScript(() => document.readyState),
        5000);
  }
}

module.exports = BasePage;
