const {setWorldConstructor, World} = require('@cucumber/cucumber');
const config = require('config');
const expect = require('chai').expect;

class CustomWorld extends World {
  constructor(options) {
    super(options);
    this.config = config;
    this.expect = expect;
  }

  async pause(durationInMs) {
    await new Promise((resolve, _) => {
      setTimeout(() => resolve('done!'), durationInMs);
    });
  }
}

setWorldConstructor(CustomWorld);
