const {After, Before, Status} = require('@cucumber/cucumber');
const seleniumWebdriver = require('selenium-webdriver');
const config = require('config');
const Chrome = require('selenium-webdriver/chrome');

Before('@browser', function() {
  const browser = config.get('browser');
  let browserOptions;

  // todo implement other browsers
  if (browser === 'chrome') {
    if (config.get('headless') === true) {
      browserOptions = new Chrome.Options().headless();
    }
  }

  this.driver = new seleniumWebdriver.Builder()
      .forBrowser(browser)
      .setChromeOptions(browserOptions)
      .build();
});

After('@browser', function() {
  this.driver.quit();
});


After(function(testCase) {
  const world = this;
  if (testCase.result.status === Status.FAILED) {
    return this.driver.takeScreenshot().then(function(screenShot) {
      world.attach(screenShot, 'base64:image/png');
    });
  }
});
