# CucumberJS - Selenium template

## prerequisites

- latest nodejs
- cucumber reports api key

## how to run

```shell
npm i
npm test
```

### cucumber reports

- register [here](https://reports.cucumber.io/)
- follow the instructions




